import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Gallery} from '../gallery.interface';

@Component({
  selector: 'mod-gallery-card',
  templateUrl: './gallery-card.component.html',
  styleUrls: ['./gallery-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GalleryCardComponent implements OnInit {
  @Input() gallery: Gallery;

  constructor() { }

  ngOnInit(): void {
  }

}
