import {Gallery} from './gallery.interface';

export class Photographer {
  name: string;
  phone?: string;
  email?: string;
  bio?: string;
  profile_picture: string;
  album?: Gallery[]
}
