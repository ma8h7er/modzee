export class Gallery {
  id: number;
  title: string;
  description?: string;
  img: string;
  date: string;
  featured: boolean;
}
