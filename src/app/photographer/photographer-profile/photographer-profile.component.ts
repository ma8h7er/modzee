import {Component, OnDestroy, OnInit} from '@angular/core';
import {PhotographerService} from '../photographer.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Photographer} from '../photographer.interface';
import {environment} from '../../../environments/environment';
import {modAnimations} from '../../animations';
import {MetaService} from '@ngx-meta/core';

@Component({
  selector: 'mod-photographer-profile',
  templateUrl: './photographer-profile.component.html',
  styleUrls: ['./photographer-profile.component.scss'],
  animations: [modAnimations]
})
export class PhotographerProfileComponent implements OnInit, OnDestroy {
  photographer: Photographer;
  private _unsubscribeAll = new Subject<any>();

  constructor(private photographerService: PhotographerService, private meta: MetaService) { }

  ngOnInit(): void {
    this.getPhotographer();
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  /**
   * Get photographer profile from JSON file or API
   */
  getPhotographer() {
    if(environment.fetchFromApi) {
      this.photographerService.getProfileFromApi(1)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((response: any) => {
            this.photographer = response.data;
            this.setMetaTags();
          },
          error => console.log(error));
    }
    else {
      this.photographerService.getDemoProfile()
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((photographer: Photographer) => {
            this.photographer = photographer;
            this.setMetaTags();
          },
          error => console.log(error));
    }
  }

  /**
   * set page meta tags for search engine optimisation
   */
  setMetaTags() {
    this.meta.setTitle(this.photographer.name);
    this.meta.setTag('author', 'Modzee');
    this.meta.setTag('web_author', 'Modzee');
    this.meta.setTag('distribution', 'global');
    this.meta.setTag('og:title', this.photographer.name);
    this.meta.setTag('og:site_name', 'Photographers');
    this.meta.setTag('og:author', 'Modzee');
    this.meta.setTag('twitter:title', this.photographer.name);
    this.meta.setTag('og:image', this.photographer.profile_picture);
    this.meta.setTag('og:image:secure_url', this.photographer.profile_picture);
    this.meta.setTag('og:image:alt', this.photographer.name);
    this.meta.setTag('twitter:image', this.photographer.profile_picture);
    this.meta.setTag('description', this.photographer.bio);
    this.meta.setTag('og:description', this.photographer.bio);
    this.meta.setTag('twitter:description', this.photographer.bio);
    this.meta.setTag('og:type', 'website');
    this.meta.setTag('twitter:card', 'summary');
  }
}
