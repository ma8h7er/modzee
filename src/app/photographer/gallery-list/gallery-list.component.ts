import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Gallery} from '../gallery.interface';
import {modAnimations} from '../../animations';

@Component({
  selector: 'mod-gallery-list',
  templateUrl: './gallery-list.component.html',
  styleUrls: ['./gallery-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [modAnimations]
})
export class GalleryListComponent implements OnInit {
  @Input() galleries: Gallery[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
