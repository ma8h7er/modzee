import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class PhotographerService {
  private _jsonURL = 'assets/landscapes.json';
  private _apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getDemoProfile(): Observable<any> {
    return this.http.get(this._jsonURL);
  }

  getProfileFromApi(id: number): Observable<any> {
    return this.http.get(`${this._apiUrl}/photographer/${id}`);
  }
}
