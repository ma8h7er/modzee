import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PhotographerProfileComponent } from './photographer/photographer-profile/photographer-profile.component';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {HttpClientModule} from '@angular/common/http';
import { GalleryListComponent } from './photographer/gallery-list/gallery-list.component';
import { GalleryCardComponent } from './photographer/gallery-card/gallery-card.component';
import {MetaModule} from '@ngx-meta/core';

@NgModule({
  declarations: [
    AppComponent,
    PhotographerProfileComponent,
    GalleryListComponent,
    GalleryCardComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    BrowserAnimationsModule,
    HttpClientModule,
    FlexModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MetaModule.forRoot(),
    FlexLayoutModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
