## Modzee photographer profile test

This is just a demo app for photographer profile. It's been built using Angular. It's ready for server-side rendering with PWA support. 

###Installation

Inside the app directory run: ```npm install```

Then to run the app: ```npm start```
